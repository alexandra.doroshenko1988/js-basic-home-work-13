/*Теоретичні питання

1. Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval(`)`.
  
Функція`setTimeout()` запускається одноразово, через зазначений проміжок часу.
  А функція `setInterval(`)` запускається неодноразово, а періодично через зазначений інтервал часу.


2. Що станеться, якщо в функцію `setTimeout()` передати нульову затримку?
Чи спрацює вона миттєво і чому?

Вона не спрацює миттєво, тому що при нульовій затримці виклик функції 
буде заплановано одразу після виконання поточного коду.


3. Чому важливо не забувати викликати функцію `clearInterval()`, 
коли раніше створений цикл запуску вам вже не потрібен?

Тому що зовнішні змінні існують в пам'яті до того часу, поки існує функція, 
вони можуть займати більше пам'ті, ніж функція. 


*/


// У папці banners лежить HTML код та папка з картинками.
// При запуску програми на екрані має відображатись перша картинка.
// Через 3 секунди замість неї має бути показано друга картинка.
// Ще через 3 секунди – третя.
// Ще через 3 секунди – четверта.
// Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
// Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.
// Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною та картинка, 
// яка була там при натисканні кнопки.
// Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває з тієї картинки, 
// яка в даний момент показана на екрані.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.

const btnStop = document.querySelector("#btn-stop");
const btnStart = document.querySelector("#btn-start");

let slideIndex = 0;
showSlides();

let interval = setInterval(showSlides, 3000);

btnStop.addEventListener("click", () => {
    clearInterval(interval);
    btnStart.removeAttribute("disabled");
    btnStop.setAttribute("disabled", true);
});
btnStart.addEventListener("click", () => {
    interval = setInterval(showSlides, 3000);
    btnStart.setAttribute("disabled", true);
    btnStop.removeAttribute("disabled");
});

function showSlides() {
    const slides = document.getElementsByClassName("image-to-show");
    const activeSlide = document.querySelector(".image-to-show.active");

    activeSlide.classList.remove("active");

    slideIndex++;
    if (slideIndex >= slides.length) {
        slideIndex = 0;
    }

    slides[slideIndex].classList.add("active");
}

